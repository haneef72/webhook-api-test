var createError = require('http-errors');
var express = require('express');
const cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const axios = require('axios');
const port = 3005;


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/', (req, res) => res.send('Webhook Test'))
app.listen(port);


app.post('/api/webhook/orderstatus',(req,res)=>{
  return res.send('Hello world')
})

const _token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwb3NUeXBlIjoiUG9zRGVtbyIsInR5cGUiOiJ0aGlyZC1wYXJ0eSIsInNlY3JldCI6Im9ubDQ3ZzVzNiIsImFkbWluU3RvcmVJZCI6bnVsbCwiaWF0IjoxNTYyMDg3MDM0LCJleHAiOjE1NjI2OTE4MzR9.Y0ouR2NYMXEPRVmjtE5Ymydpwjdv1KvWM8Z5rua9lfE';

app.post('/api/webhook/neworder', function (req, res) {

  // 1. Call our TmmmT Order API
  axios.get('https://nbeta2.tmmmt.com/api/v5/orders/' + req.body.orderId, {
    headers: { 'Authorization': _token },
  }).then(async resp => {
    if (!resp || !resp.data) {
      console.log('resp1', resp)
      // throw new Error('Invalid data');
      return res.status(400).send('Failed to fetch order data: ', resp)
    }

    const orderData = resp.data;

    console.log('Order status is ', orderData.orderStatus);

    axios.post('https://nbeta2.tmmmt.com/api/v5/orders/' + req.body.orderId, { pkid: orderData.chefPkid, orderId: orderData._id, isAccept: 1 }, {
      headers: { 'Authorization': _token },
      params: { event: 'restaurentAccept' }
    }).then(async resp2 => {
      if (!resp2 || !resp2.data) {
        console.log('resp2', resp2)
        // throw new Error('Invalid data');
        return res.status(400).send('Failed to confirm order: ', resp2)
      }

      res.status(200).send(resp2.data);
    }).catch(err => {
      console.error(err)
      res.status(400).send('Failed to fetch data: ', err.message)
    });

    // 2. Get userPkid chefPkid
    // 3. Call CHef accept API
  }).catch(err => {
    console.error(err)
    res.status(400).send('Failed to fetch data: ', err.message)
  });
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
